<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 22.03.2018
 * Time: 0:24
 */

namespace App\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;

class BaseForm
{
    const STRING_REQUIRED = "Toto pole je povinné!";

    private $container;

    /**
     * BaseForm constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param null $data
     * @param array $options
     * @return FormBuilderInterface
     */
    public function createFormBuilder($data = null, array $options = array()): FormBuilderInterface
    {
        return $this->container->get('form.factory')->createBuilder(FormType::class, $data, $options);
    }
}