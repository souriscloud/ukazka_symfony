<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 21.03.2018
 * Time: 22:44
 */

namespace App\ORM;

use Doctrine\ORM\Mapping as ORM;

trait TIdentifier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TIdentifier
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
}