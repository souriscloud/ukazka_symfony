<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 21.03.2018
 * Time: 22:48
 */

namespace App\ORM;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseEntity
 * @package App\ORM
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class BaseEntity
{
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return BaseEntity
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     * @return BaseEntity
     */
    public function setUpdated()
    {
        $this->updated = new \DateTime();
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastModification(): \DateTime
    {
        return $this->getUpdated() ?: $this->getCreated();
    }
}