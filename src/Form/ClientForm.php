<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 22.03.2018
 * Time: 0:22
 */

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ClientForm extends BaseForm
{
    /**
     * @param array|null $defaults
     * @return \Symfony\Component\Form\FormInterface
     */
    public function create(?array $defaults = null)
    {
        $form = $this->createFormBuilder($defaults)
            ->add('firstName', TextType::class, [
                "constraints" => new NotBlank(["message" => BaseForm::STRING_REQUIRED])
            ])
            ->add('lastName', TextType::class, [
                "constraints" => new NotBlank(["message" => BaseForm::STRING_REQUIRED])
            ])
            ->add('phoneNumber', TextType::class, [
                "constraints" => new Regex(["pattern" => "/^(\+\d{3})([\s-]*)(\d{3})([\s-]*)(\d{3})([\s-]*)(\d{3})$/"])
            ])
            ->add('mailAddress', EmailType::class, [
                "constraints" => new Email(["message" => "Nezadali jste platnou emailovou adresu!"])
            ])
            ->getForm();

        return $form;
    }
}