<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 21.03.2018
 * Time: 18:01
 */

namespace App\Controller;

use App\Form\ClientForm;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends Controller
{
    /**
     * @Route("/", name="test_index");
     *
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function index(ClientRepository $clientRepository)
    {
        $clients = $clientRepository->findAll();

        return $this->render('index.html.twig', ["clients" => $clients]);
    }

    /**
     * @Route("/create", name="test_create")
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ClientForm $clientForm
     * @return Response
     */
    public function create(Request $request, ClientRepository $clientRepository, ClientForm $clientForm)
    {
        $form = $clientForm->create();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $client = null;
            try {
                $client = $clientRepository->insert($data);
            } catch (\Exception $exception) {
                $this->addFlash("danger", $exception->getMessage());
            }
            if ($client !== null) {
                $this->addFlash("success", "Klient byl úspěšně vytvořen!");
            }

            return $this->redirectToRoute("test_index");
        }

        return $this->render('create.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/read/{id}", name="test_read", requirements={"id"="\d+"})
     *
     * @param $id
     */
    public function read($id)
    {
        throw new NotImplementedException("Read action not implemented!");
    }

    /**
     * @Route("/update/{id}", name="test_update", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ClientForm $clientForm
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function update($id, Request $request, ClientRepository $clientRepository, ClientForm $clientForm)
    {
        $data = $clientRepository->getFormDataById($id);

        $form = $clientForm->create($data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $client = null;
            try {
                $client = $clientRepository->update($data, $id);
            } catch (\Exception $exception) {
                $this->addFlash("danger", $exception->getMessage());
            }
            if ($client !== null) {
                $this->addFlash("success", "Klient byl úspěšně upraven!");
            }

            return $this->redirectToRoute("test_index");
        }

        return $this->render('update.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="test_delete", requirements={"id"="\d+"})
     *
     * @param $id
     * @param ClientRepository $clientRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete($id, ClientRepository $clientRepository)
    {
        try {
            if ($clientRepository->removeById($id)) {
                $this->addFlash("success", "Klient byl vymazán!");
            }
        } catch (\Exception $exception) {
            $this->addFlash("danger", $exception->getMessage());
        }

        return $this->redirectToRoute("test_index");
    }
}